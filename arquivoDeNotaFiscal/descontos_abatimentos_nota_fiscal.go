package arquivoDeNotaFiscal

type DescontosAbatimentosNotaFiscal struct {
	TipoRegistro                    string  `json:"TipoRegistro"`
	PercentualDesncontoComercial    float32 `json:"PercentualDesncontoComercial"`
	ValorDescontoComercial          float64 `json:"ValorDescontoComercial"`
	PercentualDecontoFinanceiro     float32 `json:"PercentualDecontoFinanceiro"`
	ValorDescontoFinanceiro         float64 `json:"ValorDescontoFinanceiro"`
	PercentualDescontoPromocional   float32 `json:"PercentualDescontoPromocional"`
	ValorDescontoPromocional        float64 `json:"ValorDescontoPromocional"`
	PercentualEncargoFinanceiro     float32 `json:"PercentualEncargoFinanceiro"`
	ValorEncargoFinanceiro          float64 `json:"ValorEncargoFinanceiro"`
	PercentualEncargoFrete          float32 `json:"PercentualEncargoFrete"`
	ValorEncargoFrete               float64 `json:"ValorEncargoFrete"`
	PercentualEncargoSeguro         float32 `json:"PercentualEncargoSeguro"`
	ValorEncargoSeguro              float64 `json:"ValorEncargoSeguro"`
	PercentualDescontoRepasseICMS   float32 `json:"PercentualDescontoRepasseICMS"`
	ValorDescontoRepasseICMS        float64 `json:"ValorDescontoRepasseICMS"`
	PercentualOutrosDescontos       float32 `json:"PercentualOutrosDescontos"`
	ValorOutrosDescontos            float64 `json:"ValorOutrosDescontos"`
	PercentualOutrosAbatimentos     float32 `json:"PercentualOutrosAbatimentos"`
	ValorOutrosAbatimentos          float64 `json:"ValorOutrosAbatimentos"`
	PercentualOutrosEncargos        float32 `json:"PercentualOutrosEncargos"`
	ValorOutrosEncargos             float64 `json:"ValorOutrosEncargos"`
	PercentualEncargosEmpacotamento float32 `json:"PercentualEncargosEmpacotamento"`
	ValorEncargosEmpacotamento      float64 `json:"ValorEncargosEmpacotamento"`
	ValorTotalEncargos              float64 `json:"ValorTotalEncargos"`
	Filler                          string  `json:"Filler"`
}
