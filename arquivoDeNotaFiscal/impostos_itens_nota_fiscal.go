package arquivoDeNotaFiscal

type ImpostosItensNotaFiscal struct {
	TipoRegistro                                   string  `json:"TipoRegistro"`
	Aliquota                                       float32 `json:"Aliquota"`
	PercentualReducaoICMS                          float32 `json:"PercentualReducaoICMS"`
	CodigoTipoTributacaoICMS                       string  `json:"CodigoTipoTributacaoICMS"`
	ValorBaseCalculoICMS                           float64 `json:"ValorBaseCalculoICMS"`
	ValorICMS                                      float64 `json:"ValorICMS"`
	AliquotaICMSSubstituicaoTributaria             float32 `json:"AliquotaICMSSubstituicaoTributaria"`
	CodigoTipoTributacaoICMSSubstituicaoTributaria string  `json:"CodigoTipoTributacaoICMSSubstituicaoTributaria"`
	ValorBaseCalculoICMSSubstituicaoTributariaItem float64 `json:"ValorBaseCalculoICMSSubstituicaoTributariaItem"`
	ValorICMSSubstituicaoTributariaItem            float64 `json:"ValorICMSSubstituicaoTributariaItem"`
	AliquotaIPI                                    float32 `json:"AliquotaIPI"`
	CodigoTipoTributacaoIPI                        string  `json:"CodigoTipoTributacaoIPI"`
	ValorBaseCalculoIPI                            float64 `json:"ValorBaseCalculoIPI"`
	ValorIPIItem                                   float64 `json:"ValorIPIItem"`
	Filler                                         string  `json:"Filler"`
}
