package arquivoDeNotaFiscal

type HeaderArquivo struct {
	TipoResgistro                        string `json:"TipoResgistro"`
	IdentificacaoIntercambio             string `json:"IdentificacaoIntercambio"`
	IdentificacaoCaixaPostalRemetente    string `json:"IdentificacaoCaixaPostalRemetente"`
	IdentificacaoCaixaPostalDestinatario string `json:"IdentificacaoCaixaPostalDestinatario"`
	Filler                               string `json:"Filler"`
}
