package arquivoDeNotaFiscal

type HeaderNotaFiscal struct {
	TipoRegistro                              string	`json:"TipoRegistro"`
	TipoNotaFiscal                            string	`json:"TipoNotaFiscal"`
	NumeroNotaFiscal                          int32 	`json:"NumeroNotaFiscal"`
	SerieNotaFiscal                           string	`json:"SerieNotaFiscal"`
	SubSerieNotaFiscal                        string	`json:"SubSerieNotaFiscal"`
	FuncaoMensagem                            string	`json:"FuncaoMensagem"`
	DattaDespacho                             int32 	`json:"DattaDespacho"`
	DataEntregaPrevista                       int32 	`json:"DataEntregaPrevista"`
	DataEmissaoNotaFiscal                     int32 	`json:"DataEmissaoNotaFiscal"`
	CodigoFiscalOperacoes                     string	`json:"CodigoFiscalOperacoes"`
	IdentificacaoListaPreco                   string	`json:"IdentificacaoListaPreco"`
	IdPedidoCompraEmpresaCompradora1          string	`json:"IdPedidoCompraEmpresaCompradora1"`
	IdPedidoCompraEmpresaCompradora2          string	`json:"IdPedidoCompraEmpresaCompradora2"`
	IdPedidoCompraEmpresaCOmpradora3          string	`json:"IdPedidoCompraEmpresaCOmpradora3"`
	NumeroEanBasicoCliente                    string	`json:"NumeroEanBasicoCliente"`
	NumeroEanLocalEntrega                     string	`json:"NumeroEanLocalEntrega"`
	NumeroEanEmissorNotaFiscal                string	`json:"NumeroEanEmissorNotaFiscal"`
	CGCEmitenteNotaFiscal                     int64 	`json:"CGCEmitenteNotaFiscal"`
	UFEmitenteNotaFiscal                      string	`json:"UFEmitenteNotaFiscal"`
	IEEmitenteNotaFiscal                      string	`json:"IEEmitenteNotaFiscal"`
	NumeroEanDestinatarioNFF                  string	`json:"NumeroEanDestinatarioNFF"`
	NumeroEanFornecedor                       string	`json:"NumeroEanFornecedor"`
	CGCTransportador                          int64 	`json:"CGCTransportador"`
	NomeTransportador                         string	`json:"NomeTransportador"`
	CondicaoEntrega                           string	`json:"CondicaoEntrega"`
	NumeroTotalEmbalagens                     int32 	`json:"NumeroTotalEmbalagens"`
	TipoEmbalagem                             string	`json:"TipoEmbalagem"`
	CGCComprador                              int64 	`json:"CGCComprador"`
	CGCEmpresaFornecedorNotaFiscalMercadorias int64 	`json:"CGCEmpresaFornecedorNotaFiscalMercadorias"`
	CGCPagadorNotaFiscal                      int64 	`json:"CGCPagadorNotaFiscal"`
	CGCLocalEntregaNotaFiscalMercadorias      int64 	`json:"CGCLocalEntregaNotaFiscalMercadorias"`
	Filler                                    string	`json:"Filler"`
}