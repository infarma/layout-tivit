package arquivoDeNotaFiscal

type ItensNotaFiscal struct {
	TipoRegistro                                    string  `json:"TipoRegistro"`
	NumeroLinhaItem                                 int32   `json:"NumeroLinhaItem"`
	CodigoBarraItem                                 string  `json:"CodigoBarraItem"`
	TipoCodigoItem                                  string  `json:"TipoCodigoItem"`
	CodigoClassificacaoFiscalProduto                string  `json:"CodigoClassificacaoFiscalProduto"`
	CodigoInternoFabricanteProduto                  string  `json:"CodigoInternoFabricanteProduto"`
	PesoBrutoItem                                   float64 `json:"PesoBrutoItem"`
	PesoLiquidoitem                                 float64 `json:"PesoLiquidoitem"`
	VolumeBrutoItem                                 float64 `json:"VolumeBrutoItem"`
	QualificadorTipoQuantidade                      string  `json:"QualificadorTipoQuantidade"`
	QuantidadeItem                                  float64 `json:"QuantidadeItem"`
	UnidadeMedidaQuantidadeItem                     string  `json:"UnidadeMedidaQuantidadeItem"`
	QuantidadeUnidadesConsumoUnidadeComercializacao float64 `json:"QuantidadeUnidadesConsumoUnidadeComercializacao"`
	QuantidadeEntrega                               float64 `json:"QuantidadeEntrega"`
	ValorLiquidoLinhaItem                           float64 `json:"ValorLiquidoLinhaItem"`
	ValorBrutoLinhaItem                             float64 `json:"ValorBrutoLinhaItem"`
	PrecoUnitarioBruto                              float64 `json:"PrecoUnitarioBruto"`
	PrecoUnitarioLiquido                            float64 `json:"PrecoUnitarioLiquido"`
	BasePrecoUnitario                               int64   `json:"BasePrecoUnitario"`
	UnidadeMedidaPrecoItem                          string  `json:"UnidadeMedidaPrecoItem"`
	CodigoFiscalOperacao                            string  `json:"CodigoFiscalOperacao"`
	CodigoSituacaoTributariaEstadual                string  `json:"CodigoSituacaoTributariaEstadual"`
	IdPedidoCompraEMpresaCompradora                 string  `json:"IdPedidoCompraEMpresaCompradora"`
	IdListaPreco                                    string  `json:"IdListaPreco"`
	NumeroEmbalagensItem                            int32   `json:"NumeroEmbalagensItem"`
	TipoEmbalagemItem                               string  `json:"TipoEmbalagemItem"`
	IdentificacaoLoteFabricacaoItem                 string  `json:"IdentificacaoLoteFabricacaoItem"`
	Filler                                          string  `json:"Filler"`
}
