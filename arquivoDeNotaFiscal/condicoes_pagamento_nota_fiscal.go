package arquivoDeNotaFiscal

type CondicoesPagamentoNotaFiscal struct {
	TipoRegistro                          string  `json:"TipoRegistro"`
	TipoCondicoesPagamento                string  `json:"TipoCondicoesPagamento"`
	EventoReferencia                      string  `json:"EventoReferencia"`
	Referencia                            string  `json:"Referencia"`
	TipoPeriodoCodificado                 string  `json:"TipoPeriodoCodificado"`
	NumeroDias                            int32   `json:"NumeroDias"`
	TipoDataCndicoesPagamento             string  `json:"TipoDataCndicoesPagamento"`
	DataCondicaoPagamento                 int32   `json:"DataCondicaoPagamento"`
	TipoPercentualCondicaoPagamento       string  `json:"TipoPercentualCondicaoPagamento"`
	PercentualCondicaoPagamentoMultaJuros float32 `json:"PercentualCondicaoPagamentoMultaJuros"`
	TipoValorCondicaoPagamento            string  `json:"TipoValorCondicaoPagamento"`
	ValorCondicaoPagamento                float64 `json:"ValorCondicaoPagamento"`
	Filler                                string  `json:"Filler"`
}
