package arquivoDeNotaFiscal

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type SumarioTotaisNotaFiscal struct {
	TipoRegistro                             string 	`json:"TipoRegistro"`
	NumeroItensNotaFiscal                    float64	`json:"NumeroItensNotaFiscal"`
	PesoBrutoTotalQuilos                     float64	`json:"PesoBrutoTotalQuilos"`
	NumeroTotalEmbalagensNotaFiscal          float64	`json:"NumeroTotalEmbalagensNotaFiscal"`
	CubagemTotalEmM                          float64	`json:"CubagemTotalEmM"`
	PesoLiquidoTotalEmQuilos                 float64	`json:"PesoLiquidoTotalEmQuilos"`
	ValorTotalLinhasItemMercadorias          float64	`json:"ValorTotalLinhasItemMercadorias"`
	ValorTotalNotaFiscal                     float64	`json:"ValorTotalNotaFiscal"`
	ValorPagoAntecipadamente                 float64	`json:"ValorPagoAntecipadamente"`
	ValorTotalDescontosNotaFiscal            float64	`json:"ValorTotalDescontosNotaFiscal"`
	ValorTotalEncargosNotaFiscal             float64	`json:"ValorTotalEncargosNotaFiscal"`
	BaseCalculoICMS                          float64	`json:"BaseCalculoICMS"`
	ValorTotalICMS                           float64	`json:"ValorTotalICMS"`
	BaseCalculoICMS                          float64	`json:"BaseCalculoICMS"`
	ValorTotalICMS                           float64	`json:"ValorTotalICMS"`
	BaseCalculoICMSComReducaoTributaria      float64	`json:"BaseCalculoICMSComReducaoTributaria"`
	ValorTotalICMSConReducaoTributaria       float64	`json:"ValorTotalICMSConReducaoTributaria"`
	BaseCalculoICMSParaSbstituicaoTributaria float64	`json:"BaseCalculoICMSParaSbstituicaoTributaria"`
	ValorTotalICMSSubstituicaoTributaria     float64	`json:"ValorTotalICMSSubstituicaoTributaria"`
	BaseCalculoIPI                           float64	`json:"BaseCalculoIPI"`
	ValorTotalIPI                            float64	`json:"ValorTotalIPI"`
	Filler                                   string 	`json:"Filler"`
}

func (s *SumarioTotaisNotaFiscal) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesSumarioTotaisNotaFiscal

	err = posicaoParaValor.ReturnByType(&s.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.NumeroItensNotaFiscal, "NumeroItensNotaFiscal")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.PesoBrutoTotalQuilos, "PesoBrutoTotalQuilos")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.NumeroTotalEmbalagensNotaFiscal, "NumeroTotalEmbalagensNotaFiscal")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.CubagemTotalEmM, "CubagemTotalEmM")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.PesoLiquidoTotalEmQuilos, "PesoLiquidoTotalEmQuilos")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.ValorTotalLinhasItemMercadorias, "ValorTotalLinhasItemMercadorias")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.ValorTotalNotaFiscal, "ValorTotalNotaFiscal")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.ValorPagoAntecipadamente, "ValorPagoAntecipadamente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.ValorTotalDescontosNotaFiscal, "ValorTotalDescontosNotaFiscal")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.ValorTotalEncargosNotaFiscal, "ValorTotalEncargosNotaFiscal")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.BaseCalculoICMS, "BaseCalculoICMS")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.ValorTotalICMS, "ValorTotalICMS")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.BaseCalculoICMS, "BaseCalculoICMS")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.ValorTotalICMS, "ValorTotalICMS")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.BaseCalculoICMSComReducaoTributaria, "BaseCalculoICMSComReducaoTributaria")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.ValorTotalICMSConReducaoTributaria, "ValorTotalICMSConReducaoTributaria")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.BaseCalculoICMSParaSbstituicaoTributaria, "BaseCalculoICMSParaSbstituicaoTributaria")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.ValorTotalICMSSubstituicaoTributaria, "ValorTotalICMSSubstituicaoTributaria")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.BaseCalculoIPI, "BaseCalculoIPI")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.ValorTotalIPI, "ValorTotalIPI")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.Filler, "Filler")
	if err != nil {
		return err
	}


	return err
}

var PosicoesSumarioTotaisNotaFiscal = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                      {0, 2, 0},
	"NumeroItensNotaFiscal":                      {2, 20, 3},
	"PesoBrutoTotalQuilos":                      {20, 38, 3},
	"NumeroTotalEmbalagensNotaFiscal":                      {38, 56, 3},
	"CubagemTotalEmM":                      {56, 74, 3},
	"PesoLiquidoTotalEmQuilos":                      {74, 92, 3},
	"ValorTotalLinhasItemMercadorias":                      {92, 110, 2},
	"ValorTotalNotaFiscal":                      {110, 128, 2},
	"ValorPagoAntecipadamente":                      {128, 146, 2},
	"ValorTotalDescontosNotaFiscal":                      {146, 164, 2},
	"ValorTotalEncargosNotaFiscal":                      {164, 182, 2},
	"BaseCalculoICMS":                      {182, 200, 2},
	"ValorTotalICMS":                      {200, 218, 2},
	"BaseCalculoICMS":                      {182, 200, 2},
	"ValorTotalICMS":                      {200, 218, 2},
	"BaseCalculoICMSComReducaoTributaria":                      {218, 236, 2},
	"ValorTotalICMSConReducaoTributaria":                      {236, 254, 2},
	"BaseCalculoICMSParaSbstituicaoTributaria":                      {254, 272, 2},
	"ValorTotalICMSSubstituicaoTributaria":                      {272, 290, 2},
	"BaseCalculoIPI":                      {290, 308, 2},
	"ValorTotalIPI":                      {308, 326, 2},
	"Filler":                      {326, 400, 0},
}