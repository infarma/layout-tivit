package arquivoDeNotaFiscal

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type DescontosEncargosItensNotaFiscal struct {
	TipoRegistro                    string 	`json:"TipoRegistro"`
	PercentualDescontoRepasseICMS   float64	`json:"PercentualDescontoRepasseICMS"`
	ValorDescontoRepasseICMS        float64	`json:"ValorDescontoRepasseICMS"`
	PercentualDescontoComercial     float32	`json:"PercentualDescontoComercial"`
	ValorDescontoComercial          float64	`json:"ValorDescontoComercial"`
	PercentualDescontoFinanceiro    float32	`json:"PercentualDescontoFinanceiro"`
	ValorDescontoFinanceiro         float64	`json:"ValorDescontoFinanceiro"`
	PercentualDescontoPromocional   float32	`json:"PercentualDescontoPromocional"`
	ValorDescontoPromocional        float64	`json:"ValorDescontoPromocional"`
	PercentualEncargoFinanceiro     float32	`json:"PercentualEncargoFinanceiro"`
	ValorEncargoFinanceiro          float64	`json:"ValorEncargoFinanceiro"`
	PercentualEncargoFrete          float32	`json:"PercentualEncargoFrete"`
	ValorEncargoFrete               float64	`json:"ValorEncargoFrete"`
	PercentualEncargoEmbalagem      float32	`json:"PercentualEncargoEmbalagem"`
	ValorEncargoEmbalagem           float64	`json:"ValorEncargoEmbalagem"`
	PercentualEncargoSeguro         float32	`json:"PercentualEncargoSeguro"`
	ValorEncargoSeguro              float64	`json:"ValorEncargoSeguro"`
	PercentualOutrosDescontos       float32	`json:"PercentualOutrosDescontos"`
	ValorOutrosDescontos            float64	`json:"ValorOutrosDescontos"`
	PercentualEncargosEmpacotamento float32	`json:"PercentualEncargosEmpacotamento"`
	ValorEncargosEmpacotamento      float64	`json:"ValorEncargosEmpacotamento"`
	PercentualDescontoPublicidade   float32	`json:"PercentualDescontoPublicidade"`
	ValorDesocntoPublicidade        float64	`json:"ValorDesocntoPublicidade"`
	Filler                          string 	`json:"Filler"`
}

func (d *DescontosEncargosItensNotaFiscal) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesDescontosEncargosItensNotaFiscal

	err = posicaoParaValor.ReturnByType(&d.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualDescontoRepasseICMS, "PercentualDescontoRepasseICMS")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorDescontoRepasseICMS, "ValorDescontoRepasseICMS")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualDescontoComercial, "PercentualDescontoComercial")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorDescontoComercial, "ValorDescontoComercial")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualDescontoFinanceiro, "PercentualDescontoFinanceiro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorDescontoFinanceiro, "ValorDescontoFinanceiro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualDescontoPromocional, "PercentualDescontoPromocional")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorDescontoPromocional, "ValorDescontoPromocional")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualEncargoFinanceiro, "PercentualEncargoFinanceiro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorEncargoFinanceiro, "ValorEncargoFinanceiro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualEncargoFrete, "PercentualEncargoFrete")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorEncargoFrete, "ValorEncargoFrete")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualEncargoEmbalagem, "PercentualEncargoEmbalagem")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorEncargoEmbalagem, "ValorEncargoEmbalagem")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualEncargoSeguro, "PercentualEncargoSeguro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorEncargoSeguro, "ValorEncargoSeguro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualOutrosDescontos, "PercentualOutrosDescontos")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorOutrosDescontos, "ValorOutrosDescontos")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualEncargosEmpacotamento, "PercentualEncargosEmpacotamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorEncargosEmpacotamento, "ValorEncargosEmpacotamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualDescontoPublicidade, "PercentualDescontoPublicidade")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorDesocntoPublicidade, "ValorDesocntoPublicidade")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.Filler, "Filler")
	if err != nil {
		return err
	}


	return err
}

var PosicoesDescontosEncargosItensNotaFiscal = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                      {0, 2, 0},
	"PercentualDescontoRepasseICMS":                      {2, 7, 2},
	"ValorDescontoRepasseICMS":                      {7, 25, 2},
	"PercentualDescontoComercial":                      {25, 30, 2},
	"ValorDescontoComercial":                      {30, 48, 2},
	"PercentualDescontoFinanceiro":                      {48, 53, 2},
	"ValorDescontoFinanceiro":                      {53, 71, 2},
	"PercentualDescontoPromocional":                      {71, 76, 2},
	"ValorDescontoPromocional":                      {76, 94, 2},
	"PercentualEncargoFinanceiro":                      {94, 99, 2},
	"ValorEncargoFinanceiro":                      {99, 117, 2},
	"PercentualEncargoFrete":                      {117, 122, 2},
	"ValorEncargoFrete":                      {122, 140, 2},
	"PercentualEncargoEmbalagem":                      {140, 145, 2},
	"ValorEncargoEmbalagem":                      {145, 163, 2},
	"PercentualEncargoSeguro":                      {163, 168, 2},
	"ValorEncargoSeguro":                      {168, 186, 2},
	"PercentualOutrosDescontos":                      {186, 191, 2},
	"ValorOutrosDescontos":                      {191, 209, 2},
	"PercentualEncargosEmpacotamento":                      {209, 214, 2},
	"ValorEncargosEmpacotamento":                      {214, 232, 2},
	"PercentualDescontoPublicidade":                      {232, 237, 2},
	"ValorDesocntoPublicidade":                      {237, 255, 2},
	"Filler":                      {255, 400, 0},
}