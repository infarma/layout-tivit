package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type CondicoesPagamentoPedidoCompras struct {
	TipoRegistro                    string  `json:"TipoRegistro"`
	TipoCondicaoPagamento           string  `json:"TipoCondicaoPagamento"`
	EventoReferencia                string  `json:"EventoReferencia"`
	ReferenciaTempo                 string  `json:"ReferenciaTempo"`
	TipoPeriodoCodificado           string  `json:"TipoPeriodoCodificado"`
	NumeroDias                      int32   `json:"NumeroDias"`
	TipoDataCondicaoPagamento       int32   `json:"TipoDataCondicaoPagamento"`
	DataCondicaoPagamento           int32   `json:"DataCondicaoPagamento"`
	TipoPercentualCondicaoPagamento string  `json:"TipoPercentualCondicaoPagamento"`
	PercentualCondicaoPagamento     float32 `json:"PercentualCondicaoPagamento"`
	TipoValorCondicaoPagamento      string  `json:"TipoValorCondicaoPagamento"`
	ValorCondicaoPagamento          float64 `json:"ValorCondicaoPagamento"`
	Filler                          string  `json:"Filler"`
}

func (c *CondicoesPagamentoPedidoCompras) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesCondicoesPagamentoPedidoCompras

	err = posicaoParaValor.ReturnByType(&c.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.TipoCondicaoPagamento, "TipoCondicaoPagamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.EventoReferencia, "EventoReferencia")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.ReferenciaTempo, "ReferenciaTempo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.TipoPeriodoCodificado, "TipoPeriodoCodificado")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.NumeroDias, "NumeroDias")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.TipoDataCondicaoPagamento, "TipoDataCondicaoPagamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.DataCondicaoPagamento, "DataCondicaoPagamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.TipoPercentualCondicaoPagamento, "TipoPercentualCondicaoPagamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.PercentualCondicaoPagamento, "PercentualCondicaoPagamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.TipoValorCondicaoPagamento, "TipoValorCondicaoPagamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.ValorCondicaoPagamento, "ValorCondicaoPagamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.Filler, "Filler")
	if err != nil {
		return err
	}

	return err
}

var PosicoesCondicoesPagamentoPedidoCompras = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                    {0, 2, 0},
	"TipoCondicaoPagamento":           {2, 5, 0},
	"EventoReferencia":                {5, 8, 0},
	"ReferenciaTempo":                 {8, 11, 0},
	"TipoPeriodoCodificado":           {11, 14, 0},
	"NumeroDias":                      {14, 17, 0},
	"TipoDataCondicaoPagamento":       {17, 20, 0},
	"DataCondicaoPagamento":           {20, 28, 0},
	"TipoPercentualCondicaoPagamento": {28, 31, 0},
	"PercentualCondicaoPagamento":     {31, 36, 2},
	"TipoValorCondicaoPagamento":      {36, 39, 0},
	"ValorCondicaoPagamento":          {39, 57, 2},
	"Filler":                          {57, 650, 0},
}
