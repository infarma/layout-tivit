package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type HeaderPedido struct {
	TipoRegistro                     string `json:"TipoRegistro"`
	TipoPedido1                      string `json:"TipoPedido1"`
	IdentificacaoPedido              string `json:"IdentificacaoPedido"`
	AcaoPedido                       string `json:"AcaoPedido"`
	DataEntregaFixa                  int32  `json:"DataEntregaFixa"`
	DataEntregaMaisTardeFinal        int32  `json:"DataEntregaMaisTardeFinal"`
	DataEntregaMaisCedoInicio        int32  `json:"DataEntregaMaisCedoInicio"`
	DataEmissaoPedido                int32  `json:"DataEmissaoPedido"`
	DataInicioPromocao               int32  `json:"DataInicioPromocao"`
	DataTerminoFimPromocao           int32  `json:"DataTerminoFimPromocao"`
	TipoPedido2                      string `json:"TipoPedido2"`
	TipoUsoMercadoria                string `json:"TipoUsoMercadoria"`
	InofrmacaoPromocao               string `json:"InofrmacaoPromocao"`
	NumeroListaPrecos                string `json:"NumeroListaPrecos"`
	NumeroContato                    string `json:"NumeroContato"`
	NumeroEanBasicoCliente           string `json:"NumeroEanBasicoCliente"`
	NumeroEanLocalEntrega            string `json:"NumeroEanLocalEntrega"`
	RazaoSocialLocalEntrega          string `json:"RazaoSocialLocalEntrega"`
	LogradouroLocalEntrega           string `json:"LogradouroLocalEntrega"`
	BairroDistritoLocalEntrega       string `json:"BairroDistritoLocalEntrega"`
	MunicipioLocalEntrega            string `json:"MunicipioLocalEntrega"`
	UFLocalEntrega                   string `json:"UFLocalEntrega"`
	CepLocalEntrega                  string `json:"CepLocalEntrega"`
	CGCLocalEntrega                  int64  `json:"CGCLocalEntrega"`
	InscricaoEstadualLocalEntrega    string `json:"InscricaoEstadualLocalEntrega"`
	NumeroEanLocalFaturamento        string `json:"NumeroEanLocalFaturamento"`
	NumeroEanFornecedor              string `json:"NumeroEanFornecedor"`
	ModoTransporte                   string `json:"ModoTransporte"`
	IdentificacaoTransportadora      string `json:"IdentificacaoTransportadora"`
	TipoIdentificacaoTrasportadora   string `json:"TipoIdentificacaoTrasportadora"`
	NomeTransportadora               string `json:"NomeTransportadora"`
	CondicaoEntrega                  string `json:"CondicaoEntrega"`
	DataCancelamentoPedido           int32  `json:"DataCancelamentoPedido"`
	TipoPedidoWalMart                string `json:"TipoPedidoWalMart"`
	RazaoSocialCliente               string `json:"RazaoSocialCliente"`
	RazaoSocialFornecedor            string `json:"RazaoSocialFornecedor"`
	NumeroEanDestinatario            string `json:"NumeroEanDestinatario"`
	RazaoSocialDestinatario          string `json:"RazaoSocialDestinatario"`
	MoedaCorrente                    string `json:"MoedaCorrente"`
	FormaPagamentoEncargosTransporte string `json:"FormaPagamentoEncargosTransporte"`
	Filler                           string `json:"Filler"`
}

func (h *HeaderPedido) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesHeaderPedido

	err = posicaoParaValor.ReturnByType(&h.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.TipoPedido1, "TipoPedido1")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.IdentificacaoPedido, "IdentificacaoPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.AcaoPedido, "AcaoPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.DataEntregaFixa, "DataEntregaFixa")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.DataEntregaMaisTardeFinal, "DataEntregaMaisTardeFinal")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.DataEntregaMaisCedoInicio, "DataEntregaMaisCedoInicio")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.DataEmissaoPedido, "DataEmissaoPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.DataInicioPromocao, "DataInicioPromocao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.DataTerminoFimPromocao, "DataTerminoFimPromocao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.TipoPedido2, "TipoPedido2")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.TipoUsoMercadoria, "TipoUsoMercadoria")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.InofrmacaoPromocao, "InofrmacaoPromocao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.NumeroListaPrecos, "NumeroListaPrecos")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.NumeroContato, "NumeroContato")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.NumeroEanBasicoCliente, "NumeroEanBasicoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.NumeroEanLocalEntrega, "NumeroEanLocalEntrega")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.RazaoSocialLocalEntrega, "RazaoSocialLocalEntrega")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.LogradouroLocalEntrega, "LogradouroLocalEntrega")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.BairroDistritoLocalEntrega, "BairroDistritoLocalEntrega")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.MunicipioLocalEntrega, "MunicipioLocalEntrega")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.UFLocalEntrega, "UFLocalEntrega")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.CepLocalEntrega, "CepLocalEntrega")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.CGCLocalEntrega, "CGCLocalEntrega")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.InscricaoEstadualLocalEntrega, "InscricaoEstadualLocalEntrega")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.NumeroEanLocalFaturamento, "NumeroEanLocalFaturamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.NumeroEanFornecedor, "NumeroEanFornecedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.ModoTransporte, "ModoTransporte")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.IdentificacaoTransportadora, "IdentificacaoTransportadora")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.TipoIdentificacaoTrasportadora, "TipoIdentificacaoTrasportadora")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.NomeTransportadora, "NomeTransportadora")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.CondicaoEntrega, "CondicaoEntrega")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.DataCancelamentoPedido, "DataCancelamentoPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.TipoPedidoWalMart, "TipoPedidoWalMart")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.RazaoSocialCliente, "RazaoSocialCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.RazaoSocialFornecedor, "RazaoSocialFornecedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.NumeroEanDestinatario, "NumeroEanDestinatario")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.RazaoSocialDestinatario, "RazaoSocialDestinatario")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.MoedaCorrente, "MoedaCorrente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.FormaPagamentoEncargosTransporte, "FormaPagamentoEncargosTransporte")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.Filler, "Filler")
	if err != nil {
		return err
	}

	return err
}

var PosicoesHeaderPedido = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                     {0, 2, 0},
	"TipoPedido1":                      {2, 5, 0},
	"IdentificacaoPedido":              {5, 25, 0},
	"AcaoPedido":                       {25, 28, 0},
	"DataEntregaFixa":                  {28, 36, 0},
	"DataEntregaMaisTardeFinal":        {36, 44, 0},
	"DataEntregaMaisCedoInicio":        {44, 52, 0},
	"DataEmissaoPedido":                {52, 60, 0},
	"DataInicioPromocao":               {60, 68, 0},
	"DataTerminoFimPromocao":           {68, 76, 0},
	"TipoPedido2":                      {76, 79, 0},
	"TipoUsoMercadoria":                {79, 82, 0},
	"InofrmacaoPromocao":               {82, 102, 0},
	"NumeroListaPrecos":                {102, 122, 0},
	"NumeroContato":                    {122, 142, 0},
	"NumeroEanBasicoCliente":           {142, 155, 0},
	"NumeroEanLocalEntrega":            {155, 168, 0},
	"RazaoSocialLocalEntrega":          {168, 203, 0},
	"LogradouroLocalEntrega":           {203, 238, 0},
	"BairroDistritoLocalEntrega":       {238, 273, 0},
	"MunicipioLocalEntrega":            {273, 308, 0},
	"UFLocalEntrega":                   {308, 310, 0},
	"CepLocalEntrega":                  {310, 319, 0},
	"CGCLocalEntrega":                  {319, 334, 0},
	"InscricaoEstadualLocalEntrega":    {334, 354, 0},
	"NumeroEanLocalFaturamento":        {354, 367, 0},
	"NumeroEanFornecedor":              {367, 380, 0},
	"ModoTransporte":                   {380, 383, 0},
	"IdentificacaoTransportadora":      {383, 397, 0},
	"TipoIdentificacaoTrasportadora":   {397, 400, 0},
	"NomeTransportadora":               {400, 435, 0},
	"CondicaoEntrega":                  {435, 438, 0},
	"DataCancelamentoPedido":           {438, 446, 0},
	"TipoPedidoWalMart":                {446, 481, 0},
	"RazaoSocialCliente":               {481, 516, 0},
	"RazaoSocialFornecedor":            {516, 551, 0},
	"NumeroEanDestinatario":            {551, 564, 0},
	"RazaoSocialDestinatario":          {564, 599, 0},
	"MoedaCorrente":                    {599, 602, 0},
	"FormaPagamentoEncargosTransporte": {602, 605, 0},
	"Filler":                           {605, 650, 0},
}
