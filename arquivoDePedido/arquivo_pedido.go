package arquivoDePedido

import (
	"bufio"
	"os"
)

type ArquivoDePedido struct {
	HeaderArquivo                   HeaderArquivo                     `json:"HeaderArquivo"`
	HeaderPedido                    HeaderPedido                      `json:"HeaderPedido"`
	TextoLivre                      TextoLivre                        `json:"TextoLivre"`
	CondicoesPagamentoPedidoCompras []CondicoesPagamentoPedidoCompras `json:"CondicoesPagamentoPedidoCompras"`
	DescontosEncargosPedidoCompras  []DescontosEncargosPedidoCompras  `json:"DescontosEncargosPedidoCompras"`
	ItensPedido                     []ItensPedido                     `json:"ItensPedido"`
	TestoLivreItensPedido           []TestoLivreItensPedido           `json:"TestoLivreItensPedido"`
	CrossDocking                    []CrossDocking                    `json:"CrossDocking"`
	DescontosEncargosItensPedido    []DescontosEncargosItensPedido    `json:"DescontosEncargosItensPedido"`
	SumarioTotaisPedido             SumarioTotaisPedido               `json:"SumarioTotaisPedido"`
}

func GetStruct(fileHandle *os.File) (ArquivoDePedido, error) {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoDePedido{}
	var err error
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		identificador := string(runes[0:2])

		var indexCondicoesPagamentoPedidoCompras, indexDescontosEncargosPedidoCompras, indexItensPedido,
		indexTestoLivreItensPedido, indexCrossDocking, indexDescontosEncargosItensPedido int32
		if identificador == "00" {
			err = arquivo.HeaderArquivo.ComposeStruct(string(runes))
		} else if identificador == "01" {
			err = arquivo.HeaderPedido.ComposeStruct(string(runes))
		} else if identificador == "1ª" {
			err = arquivo.TextoLivre.ComposeStruct(string(runes))
		} else if identificador == "02" {
			err = arquivo.CondicoesPagamentoPedidoCompras[indexCondicoesPagamentoPedidoCompras].ComposeStruct(string(runes))
			indexCondicoesPagamentoPedidoCompras++
		} else if identificador == "03" {
			err = arquivo.DescontosEncargosPedidoCompras[indexDescontosEncargosPedidoCompras].ComposeStruct(string(runes))
			indexDescontosEncargosPedidoCompras++
		} else if identificador == "05" {
			err = arquivo.ItensPedido[indexItensPedido].ComposeStruct(string(runes))
			indexItensPedido++
		} else if identificador == "5A" {
			err = arquivo.TestoLivreItensPedido[indexTestoLivreItensPedido].ComposeStruct(string(runes))
			indexTestoLivreItensPedido++
		} else if identificador == "06" {
			err = arquivo.CrossDocking[indexCrossDocking].ComposeStruct(string(runes))
			indexCrossDocking++
		} else if identificador == "07" {
			err = arquivo.DescontosEncargosItensPedido[indexDescontosEncargosItensPedido].ComposeStruct(string(runes))
			indexDescontosEncargosItensPedido++
		} else if identificador == "09" {
			err = arquivo.SumarioTotaisPedido.ComposeStruct(string(runes))
		}
	}
	return arquivo, err
}
