package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type HeaderArquivo struct {
	TipoRegistro                         string `json:"TipoRegistro"`
	IdentificacaoIntercambioArquivo      string `json:"IdentificacaoIntercambioArquivo"`
	IdentificacaoCaixaPostalRemetente    string `json:"IdentificacaoCaixaPostalRemetente"`
	IdentificacaoCaixaPostalDestinatario string `json:"IdentificacaoCaixaPostalDestinatario"`
	Filler                               string `json:"Filler"`
}

func (h *HeaderArquivo) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesHeaderArquivo

	err = posicaoParaValor.ReturnByType(&h.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.IdentificacaoIntercambioArquivo, "IdentificacaoIntercambioArquivo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.IdentificacaoCaixaPostalRemetente, "IdentificacaoCaixaPostalRemetente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.IdentificacaoCaixaPostalDestinatario, "IdentificacaoCaixaPostalDestinatario")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.Filler, "Filler")
	if err != nil {
		return err
	}

	return err
}

var PosicoesHeaderArquivo = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                         {0, 2, 0},
	"IdentificacaoIntercambioArquivo":      {2, 16, 0},
	"IdentificacaoCaixaPostalRemetente":    {16, 51, 0},
	"IdentificacaoCaixaPostalDestinatario": {51, 86, 0},
	"Filler":                               {86, 650, 0},
}
