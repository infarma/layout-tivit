package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type ItensPedido struct {
	TipoRegistro                                  string  `json:"TipoRegistro"`
	NumeroLinhaItem                               int32   `json:"NumeroLinhaItem"`
	CodigoBarraItemProduto                        string  `json:"CodigoBarraItemProduto"`
	TipoCodigoItemProduto                         string  `json:"TipoCodigoItemProduto"`
	CodigoInternoFabricanteProduto                string  `json:"CodigoInternoFabricanteProduto"`
	QuantidadeSerFaturadaItemProduto              int64   `json:"QuantidadeSerFaturadaItemProduto"`
	UnidadeMedidaQuantidadeItemProdutoSerFaturada string  `json:"UnidadeMedidaQuantidadeItemProdutoSerFaturada"`
	QuantidadeGratuitaBonificada                  int64   `json:"QuantidadeGratuitaBonificada"`
	UnidadeMedidaQuantidadeGratuitaBonificada     string  `json:"UnidadeMedidaQuantidadeGratuitaBonificada"`
	NumeroUnidadeConsumo                          int64   `json:"NumeroUnidadeConsumo"`
	NumeroUnidadesEntrega                         int64   `json:"NumeroUnidadesEntrega"`
	PrecoBrutoUnitario                            float64 `json:"PrecoBrutoUnitario"`
	PrecoLiquidoUnitario                          float64 `json:"PrecoLiquidoUnitario"`
	BasePrecoBrutoUnitario                        float64 `json:"BasePrecoBrutoUnitario"`
	UnidadeMedidaPrecoItemProduto                 string  `json:"UnidadeMedidaPrecoItemProduto"`
	NumeroEmbalagens                              int32   `json:"NumeroEmbalagens"`
	IndicadorSublinhaItem                         string  `json:"IndicadorSublinhaItem"`
	NumeroSublinhaItem                            int32   `json:"NumeroSublinhaItem"`
	StatusConfiguracao                            string  `json:"StatusConfiguracao"`
	AcaoCodigoItem                                string  `json:"AcaoCodigoItem"`
	TipoIdentificacaoItem                         string  `json:"TipoIdentificacaoItem"`
	CaracteristicasItem                           string  `json:"CaracteristicasItem"`
	IdentificacaoDescricaoItem                    string  `json:"IdentificacaoDescricaoItem"`
	ValorItem                                     float64 `json:"ValorItem"`
	NivelEmbalagens                               string  `json:"NivelEmbalagens"`
	AliquotaIPI                                   float32 `json:"AliquotaIPI"`
	ValorIPI                                      float64 `json:"ValorIPI"`
	Filler                                        string  `json:"Filler"`
}

func (i *ItensPedido) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesItensPedido

	err = posicaoParaValor.ReturnByType(&i.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.NumeroLinhaItem, "NumeroLinhaItem")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.CodigoBarraItemProduto, "CodigoBarraItemProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.TipoCodigoItemProduto, "TipoCodigoItemProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.CodigoInternoFabricanteProduto, "CodigoInternoFabricanteProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.QuantidadeSerFaturadaItemProduto, "QuantidadeSerFaturadaItemProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.UnidadeMedidaQuantidadeItemProdutoSerFaturada, "UnidadeMedidaQuantidadeItemProdutoSerFaturada")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.QuantidadeGratuitaBonificada, "QuantidadeGratuitaBonificada")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.UnidadeMedidaQuantidadeGratuitaBonificada, "UnidadeMedidaQuantidadeGratuitaBonificada")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.NumeroUnidadeConsumo, "NumeroUnidadeConsumo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.NumeroUnidadesEntrega, "NumeroUnidadesEntrega")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.PrecoBrutoUnitario, "PrecoBrutoUnitario")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.PrecoLiquidoUnitario, "PrecoLiquidoUnitario")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.BasePrecoBrutoUnitario, "BasePrecoBrutoUnitario")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.UnidadeMedidaPrecoItemProduto, "UnidadeMedidaPrecoItemProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.NumeroEmbalagens, "NumeroEmbalagens")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.IndicadorSublinhaItem, "IndicadorSublinhaItem")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.NumeroSublinhaItem, "NumeroSublinhaItem")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.StatusConfiguracao, "StatusConfiguracao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.AcaoCodigoItem, "AcaoCodigoItem")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.TipoIdentificacaoItem, "TipoIdentificacaoItem")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.CaracteristicasItem, "CaracteristicasItem")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.IdentificacaoDescricaoItem, "IdentificacaoDescricaoItem")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.ValorItem, "ValorItem")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.NivelEmbalagens, "NivelEmbalagens")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.AliquotaIPI, "AliquotaIPI")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.ValorIPI, "ValorIPI")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.Filler, "Filler")
	if err != nil {
		return err
	}

	return err
}

var PosicoesItensPedido = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                                  {0, 2, 0},
	"NumeroLinhaItem":                               {2, 8, 0},
	"CodigoBarraItemProduto":                        {8, 22, 0},
	"TipoCodigoItemProduto":                         {22, 25, 0},
	"CodigoInternoFabricanteProduto":                {25, 45, 0},
	"QuantidadeSerFaturadaItemProduto":              {45, 60, 0},
	"UnidadeMedidaQuantidadeItemProdutoSerFaturada": {60, 63, 0},
	"QuantidadeGratuitaBonificada":                  {63, 78, 0},
	"UnidadeMedidaQuantidadeGratuitaBonificada":     {78, 81, 0},
	"NumeroUnidadeConsumo":                          {81, 96, 0},
	"NumeroUnidadesEntrega":                         {96, 111, 0},
	"PrecoBrutoUnitario":                            {111, 126, 2},
	"PrecoLiquidoUnitario":                          {126, 141, 2},
	"BasePrecoBrutoUnitario":                        {141, 150, 2},
	"UnidadeMedidaPrecoItemProduto":                 {150, 153, 0},
	"NumeroEmbalagens":                              {153, 161, 0},
	"IndicadorSublinhaItem":                         {161, 164, 0},
	"NumeroSublinhaItem":                            {164, 170, 0},
	"StatusConfiguracao":                            {170, 173, 0},
	"AcaoCodigoItem":                                {173, 176, 0},
	"TipoIdentificacaoItem":                         {176, 179, 0},
	"CaracteristicasItem":                           {179, 182, 0},
	"IdentificacaoDescricaoItem":                    {182, 199, 0},
	"ValorItem":                                     {199, 217, 2},
	"NivelEmbalagens":                               {217, 222, 0},
	"AliquotaIPI":                                   {222, 224, 2},
	"ValorIPI":                                      {224, 242, 2},
	"Filler":                                        {242, 660, 0},
}
