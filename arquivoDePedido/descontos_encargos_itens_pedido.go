package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type DescontosEncargosItensPedido struct {
	TipRegistro                                     string  `json:"TipRegistro"`
	PercentualDescontoComercial                     float32 `json:"PercentualDescontoComercial"`
	ValorDescontoComercial                          float64 `json:"ValorDescontoComercial"`
	PercentualDescontoFinanceiro                    float32 `json:"PercentualDescontoFinanceiro"`
	ValorDescontoFinanceiro                         float64 `json:"ValorDescontoFinanceiro"`
	PercentualDescontoPromocional                   float32 `json:"PercentualDescontoPromocional"`
	ValorDescontoPromocional                        float32 `json:"ValorDescontoPromocional"`
	PercentualEncargoFinanceiro                     float32 `json:"PercentualEncargoFinanceiro"`
	ValorEncargoFinanceiro                          float64 `json:"ValorEncargoFinanceiro"`
	PercentualEncargoFrete                          float32 `json:"PercentualEncargoFrete"`
	ValorEncargoFrete                               float64 `json:"ValorEncargoFrete"`
	PercentualEncargoEmbalagem                      float32 `json:"PercentualEncargoEmbalagem"`
	ValorEncargoEmbalagem                           float64 `json:"ValorEncargoEmbalagem"`
	PercentualEncargoSeguro                         float32 `json:"PercentualEncargoSeguro"`
	ValorEncargoSeguro                              float32 `json:"ValorEncargoSeguro"`
	CodigoAcertoConta                               string  `json:"CodigoAcertoConta"`
	PercentualAbatimentoFrete                       float32 `json:"PercentualAbatimentoFrete"`
	ValorAbatimentoFrete                            float64 `json:"ValorAbatimentoFrete"`
	PercentualEncargoColeta                         float32 `json:"PercentualEncargoColeta"`
	ValorEncargoColeta                              float64 `json:"ValorEncargoColeta"`
	PercentualBonificacao                           float32 `json:"PercentualBonificacao"`
	ValorBonificacao                                float64 `json:"ValorBonificacao"`
	PercentualMercadoriasEntregeSemPedido           float32 `json:"PercentualMercadoriasEntregeSemPedido"`
	ValorMercadoriasEntregueSemPedido               float64 `json:"ValorMercadoriasEntregueSemPedido"`
	PercentualDescontoPropaganda                    float32 `json:"PercentualDescontoPropaganda"`
	ValorDescontoPropaganda                         float64 `json:"ValorDescontoPropaganda"`
	PercentualDescontoDistribuidor                  float32 `json:"PercentualDescontoDistribuidor"`
	ValorDescontoDistribuidor                       float64 `json:"ValorDescontoDistribuidor"`
	PercentualDescontoNegocioOuVolume               float32 `json:"PercentualDescontoNegocioOuVolume"`
	ValorDescontoNegocioOuVolume                    float64 `json:"ValorDescontoNegocioOuVolume"`
	PercentualDescontoMercadoriasComRestricoesVenda float32 `json:"PercentualDescontoMercadoriasComRestricoesVenda"`
	ValorDesncontoMercadoriasComRestricoesVenda     float64 `json:"ValorDesncontoMercadoriasComRestricoesVenda"`
	ValorOurasDespesasWalmart                       float64 `json:"ValorOurasDespesasWalmart"`
	ValorEncargoFretePorUnidadeWalmart              float64 `json:"ValorEncargoFretePorUnidadeWalmart"`
	ValorDescontoPublicidadePorUnidadeWalmart       float64 `json:"ValorDescontoPublicidadePorUnidadeWalmart"`
	ValorOutrasDespesasPorUnidadeWalmart            float64 `json:"ValorOutrasDespesasPorUnidadeWalmart"`
	Filler                                          string  `json:"Filler"`
}

func (d *DescontosEncargosItensPedido) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesDescontosEncargosItensPedido

	err = posicaoParaValor.ReturnByType(&d.TipRegistro, "TipRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualDescontoComercial, "PercentualDescontoComercial")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorDescontoComercial, "ValorDescontoComercial")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualDescontoFinanceiro, "PercentualDescontoFinanceiro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorDescontoFinanceiro, "ValorDescontoFinanceiro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualDescontoPromocional, "PercentualDescontoPromocional")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorDescontoPromocional, "ValorDescontoPromocional")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualEncargoFinanceiro, "PercentualEncargoFinanceiro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorEncargoFinanceiro, "ValorEncargoFinanceiro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualEncargoFrete, "PercentualEncargoFrete")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorEncargoFrete, "ValorEncargoFrete")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualEncargoEmbalagem, "PercentualEncargoEmbalagem")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorEncargoEmbalagem, "ValorEncargoEmbalagem")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualEncargoSeguro, "PercentualEncargoSeguro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorEncargoSeguro, "ValorEncargoSeguro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.CodigoAcertoConta, "CodigoAcertoConta")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualAbatimentoFrete, "PercentualAbatimentoFrete")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorAbatimentoFrete, "ValorAbatimentoFrete")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualEncargoColeta, "PercentualEncargoColeta")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorEncargoColeta, "ValorEncargoColeta")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualBonificacao, "PercentualBonificacao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorBonificacao, "ValorBonificacao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualMercadoriasEntregeSemPedido, "PercentualMercadoriasEntregeSemPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorMercadoriasEntregueSemPedido, "ValorMercadoriasEntregueSemPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualDescontoPropaganda, "PercentualDescontoPropaganda")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorDescontoPropaganda, "ValorDescontoPropaganda")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualDescontoDistribuidor, "PercentualDescontoDistribuidor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorDescontoDistribuidor, "ValorDescontoDistribuidor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualDescontoNegocioOuVolume, "PercentualDescontoNegocioOuVolume")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorDescontoNegocioOuVolume, "ValorDescontoNegocioOuVolume")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualDescontoMercadoriasComRestricoesVenda, "PercentualDescontoMercadoriasComRestricoesVenda")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorDesncontoMercadoriasComRestricoesVenda, "ValorDesncontoMercadoriasComRestricoesVenda")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorOurasDespesasWalmart, "ValorOurasDespesasWalmart")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorEncargoFretePorUnidadeWalmart, "ValorEncargoFretePorUnidadeWalmart")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorDescontoPublicidadePorUnidadeWalmart, "ValorDescontoPublicidadePorUnidadeWalmart")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorOutrasDespesasPorUnidadeWalmart, "ValorOutrasDespesasPorUnidadeWalmart")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.Filler, "Filler")
	if err != nil {
		return err
	}

	return err
}

var PosicoesDescontosEncargosItensPedido = map[string]gerador_layouts_posicoes.Posicao{
	"TipRegistro":                                     {0, 2, 0},
	"PercentualDescontoComercial":                     {2, 7, 2},
	"ValorDescontoComercial":                          {7, 25, 2},
	"PercentualDescontoFinanceiro":                    {25, 30, 2},
	"ValorDescontoFinanceiro":                         {30, 48, 2},
	"PercentualDescontoPromocional":                   {48, 53, 2},
	"ValorDescontoPromocional":                        {53, 71, 2},
	"PercentualEncargoFinanceiro":                     {71, 76, 2},
	"ValorEncargoFinanceiro":                          {76, 94, 2},
	"PercentualEncargoFrete":                          {94, 99, 2},
	"ValorEncargoFrete":                               {99, 117, 2},
	"PercentualEncargoEmbalagem":                      {117, 122, 2},
	"ValorEncargoEmbalagem":                           {122, 140, 2},
	"PercentualEncargoSeguro":                         {140, 145, 2},
	"ValorEncargoSeguro":                              {145, 163, 2},
	"CodigoAcertoConta":                               {163, 166, 0},
	"PercentualAbatimentoFrete":                       {166, 171, 2},
	"ValorAbatimentoFrete":                            {171, 189, 2},
	"PercentualEncargoColeta":                         {189, 194, 2},
	"ValorEncargoColeta":                              {194, 212, 2},
	"PercentualBonificacao":                           {212, 217, 2},
	"ValorBonificacao":                                {217, 235, 2},
	"PercentualMercadoriasEntregeSemPedido":           {235, 240, 2},
	"ValorMercadoriasEntregueSemPedido":               {240, 258, 2},
	"PercentualDescontoPropaganda":                    {258, 263, 2},
	"ValorDescontoPropaganda":                         {263, 281, 2},
	"PercentualDescontoDistribuidor":                  {281, 286, 2},
	"ValorDescontoDistribuidor":                       {286, 304, 2},
	"PercentualDescontoNegocioOuVolume":               {304, 309, 2},
	"ValorDescontoNegocioOuVolume":                    {309, 327, 2},
	"PercentualDescontoMercadoriasComRestricoesVenda": {327, 332, 2},
	"ValorDesncontoMercadoriasComRestricoesVenda":     {332, 350, 2},
	"ValorOurasDespesasWalmart":                       {350, 368, 2},
	"ValorEncargoFretePorUnidadeWalmart":              {368, 386, 2},
	"ValorDescontoPublicidadePorUnidadeWalmart":       {386, 404, 2},
	"ValorOutrasDespesasPorUnidadeWalmart":            {404, 422, 2},
	"Filler":                                          {422, 632, 0},
}
