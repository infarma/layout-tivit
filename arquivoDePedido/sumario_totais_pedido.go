package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type SumarioTotaisPedido struct {
	TipoRegistro               string  `json:"TipoRegistro"`
	ValorTotalLinhasItem       float64 `json:"ValorTotalLinhasItem"`
	ValorTotalPedido           float64 `json:"ValorTotalPedido"`
	ValorPagoAntecipadamente   float64 `json:"ValorPagoAntecipadamente"`
	ValorTotalDesccontosPedido float64 `json:"ValorTotalDesccontosPedido"`
	ValorTotalEncargosPedido   float64 `json:"ValorTotalEncargosPedido"`
	NumeroItensPedido          float64 `json:"NumeroItensPedido"`
	Filler                     string  `json:"Filler"`
}

func (s *SumarioTotaisPedido) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesSumarioTotaisPedido

	err = posicaoParaValor.ReturnByType(&s.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.ValorTotalLinhasItem, "ValorTotalLinhasItem")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.ValorTotalPedido, "ValorTotalPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.ValorPagoAntecipadamente, "ValorPagoAntecipadamente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.ValorTotalDesccontosPedido, "ValorTotalDesccontosPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.ValorTotalEncargosPedido, "ValorTotalEncargosPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.NumeroItensPedido, "NumeroItensPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.Filler, "Filler")
	if err != nil {
		return err
	}

	return err
}

var PosicoesSumarioTotaisPedido = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":               {0, 2, 0},
	"ValorTotalLinhasItem":       {2, 20, 2},
	"ValorTotalPedido":           {20, 38, 2},
	"ValorPagoAntecipadamente":   {38, 56, 2},
	"ValorTotalDesccontosPedido": {56, 74, 2},
	"ValorTotalEncargosPedido":   {74, 92, 2},
	"NumeroItensPedido":          {92, 110, 3},
	"Filler":                     {110, 650, 0},
}
