package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type CrossDocking struct {
	TipoRegistro             string  `json:"TipoRegistro"`
	CodigoLocalEntrega       string  `json:"CodigoLocalEntrega"`
	QuantidadeSerEntregue    float64 `json:"QuantidadeSerEntregue"`
	UnidadeMedidaItemProduto string  `json:"UnidadeMedidaItemProduto"`
	Filler                   string  `json:"Filler"`
}

func (c *CrossDocking) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesCrossDocking

	err = posicaoParaValor.ReturnByType(&c.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CodigoLocalEntrega, "CodigoLocalEntrega")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.QuantidadeSerEntregue, "QuantidadeSerEntregue")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.UnidadeMedidaItemProduto, "UnidadeMedidaItemProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.Filler, "Filler")
	if err != nil {
		return err
	}

	return err
}

var PosicoesCrossDocking = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":             {0, 2, 0},
	"CodigoLocalEntrega":       {2, 15, 0},
	"QuantidadeSerEntregue":    {15, 30, 3},
	"UnidadeMedidaItemProduto": {30, 33, 0},
	"Filler":                   {33, 650, 0},
}
