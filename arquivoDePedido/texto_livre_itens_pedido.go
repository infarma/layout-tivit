package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type TestoLivreItensPedido struct {
	TipoRegistro        string `json:"TipoRegistro"`
	TipoInformacaoTesto string `json:"TipoInformacaoTesto"`
	CampoInformacoes1   string `json:"CampoInformacoes1"`
	CampoInformacoes2   string `json:"CampoInformacoes2"`
	CampoInformacao3    string `json:"CampoInformacao3"`
	CampoInformacoes4   string `json:"CampoInformacoes4"`
	CampoInformacoes5   string `json:"CampoInformacoes5"`
	IdiomaInformacoes   string `json:"IdiomaInformacoes"`
	Filler              string `json:"Filler"`
}

func (t *TestoLivreItensPedido) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesTestoLivreItensPedido

	err = posicaoParaValor.ReturnByType(&t.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.TipoInformacaoTesto, "TipoInformacaoTesto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.CampoInformacoes1, "CampoInformacoes1")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.CampoInformacoes2, "CampoInformacoes2")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.CampoInformacao3, "CampoInformacao3")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.CampoInformacoes4, "CampoInformacoes4")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.CampoInformacoes5, "CampoInformacoes5")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.IdiomaInformacoes, "IdiomaInformacoes")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.Filler, "Filler")
	if err != nil {
		return err
	}

	return err
}

var PosicoesTestoLivreItensPedido = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":        {0, 2, 0},
	"TipoInformacaoTesto": {2, 5, 0},
	"CampoInformacoes1":   {5, 75, 0},
	"CampoInformacoes2":   {75, 145, 0},
	"CampoInformacao3":    {145, 215, 0},
	"CampoInformacoes4":   {215, 285, 0},
	"CampoInformacoes5":   {285, 355, 0},
	"IdiomaInformacoes":   {355, 358, 0},
	"Filler":              {358, 650, 0},
}
