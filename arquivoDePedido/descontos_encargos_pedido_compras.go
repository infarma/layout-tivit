package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type DescontosEncargosPedidoCompras struct {
	TipoRegistro                                    string  `json:"TipoRegistro"`
	PercentualDescontoComercial                     float32 `json:"PercentualDescontoComercial"`
	ValorDescontoComercial                          float64 `json:"ValorDescontoComercial"`
	PercentualDescontoFinanceiro                    float32 `json:"PercentualDescontoFinanceiro"`
	ValorDescontoFinanceiro                         float64 `json:"ValorDescontoFinanceiro"`
	PercentualDescontoPromocional                   float32 `json:"PercentualDescontoPromocional"`
	ValorDescontoPromocional                        float64 `json:"ValorDescontoPromocional"`
	PercentualEncargoFinanceiro                     float32 `json:"PercentualEncargoFinanceiro"`
	ValorEncargoFinanceiro                          float64 `json:"ValorEncargoFinanceiro"`
	PercentualEncargoFrete                          float32 `json:"PercentualEncargoFrete"`
	ValorEncargoFrete                               float64 `json:"ValorEncargoFrete"`
	PercentualEncargoSeguro                         float32 `json:"PercentualEncargoSeguro"`
	ValorEncargoSeguro                              float64 `json:"ValorEncargoSeguro"`
	CodigoAcertoConta                               string  `json:"CodigoAcertoConta"`
	PercentualAbatimentoFrente                      float32 `json:"PercentualAbatimentoFrente"`
	ValorAbatimentoFrente                           float64 `json:"ValorAbatimentoFrente"`
	PercentualEncargoColeta                         float32 `json:"PercentualEncargoColeta"`
	ValorEncargoColeta                              float64 `json:"ValorEncargoColeta"`
	PercentualBonificacao                           float32 `json:"PercentualBonificacao"`
	ValorBonificacao                                float64 `json:"ValorBonificacao"`
	PercentualMercadoriasEntregueSemPedido          float32 `json:"PercentualMercadoriasEntregueSemPedido"`
	ValorMercadoriasEntregueSemPedido               float64 `json:"ValorMercadoriasEntregueSemPedido"`
	PercentualDescontoPropaganda                    float32 `json:"PercentualDescontoPropaganda"`
	ValorDescontoPropaganda                         float64 `json:"ValorDescontoPropaganda"`
	PercentualDescontoDistribuidor                  float32 `json:"PercentualDescontoDistribuidor"`
	ValorDescontoDistribuidor                       float64 `json:"ValorDescontoDistribuidor"`
	PercentualDescontoNegocioOuVolume               float32 `json:"PercentualDescontoNegocioOuVolume"`
	ValorDescontoNegocioOuVolume                    float64 `json:"ValorDescontoNegocioOuVolume"`
	PercentualDescontoMercadoriasComRestricoesVenda float32 `json:"PercentualDescontoMercadoriasComRestricoesVenda"`
	ValorDescontoMercadoriasComRestricoesVenda      float64 `json:"ValorDescontoMercadoriasComRestricoesVenda"`
	Filler                                          string  `json:"Filler"`
}

func (d *DescontosEncargosPedidoCompras) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesDescontosEncargosPedidoCompras

	err = posicaoParaValor.ReturnByType(&d.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualDescontoComercial, "PercentualDescontoComercial")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorDescontoComercial, "ValorDescontoComercial")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualDescontoFinanceiro, "PercentualDescontoFinanceiro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorDescontoFinanceiro, "ValorDescontoFinanceiro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualDescontoPromocional, "PercentualDescontoPromocional")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorDescontoPromocional, "ValorDescontoPromocional")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualEncargoFinanceiro, "PercentualEncargoFinanceiro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorEncargoFinanceiro, "ValorEncargoFinanceiro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualEncargoFrete, "PercentualEncargoFrete")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorEncargoFrete, "ValorEncargoFrete")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualEncargoSeguro, "PercentualEncargoSeguro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorEncargoSeguro, "ValorEncargoSeguro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.CodigoAcertoConta, "CodigoAcertoConta")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualAbatimentoFrente, "PercentualAbatimentoFrente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorAbatimentoFrente, "ValorAbatimentoFrente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualEncargoColeta, "PercentualEncargoColeta")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorEncargoColeta, "ValorEncargoColeta")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualBonificacao, "PercentualBonificacao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorBonificacao, "ValorBonificacao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualMercadoriasEntregueSemPedido, "PercentualMercadoriasEntregueSemPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorMercadoriasEntregueSemPedido, "ValorMercadoriasEntregueSemPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualDescontoPropaganda, "PercentualDescontoPropaganda")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorDescontoPropaganda, "ValorDescontoPropaganda")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualDescontoDistribuidor, "PercentualDescontoDistribuidor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorDescontoDistribuidor, "ValorDescontoDistribuidor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualDescontoNegocioOuVolume, "PercentualDescontoNegocioOuVolume")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorDescontoNegocioOuVolume, "ValorDescontoNegocioOuVolume")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.PercentualDescontoMercadoriasComRestricoesVenda, "PercentualDescontoMercadoriasComRestricoesVenda")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.ValorDescontoMercadoriasComRestricoesVenda, "ValorDescontoMercadoriasComRestricoesVenda")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&d.Filler, "Filler")
	if err != nil {
		return err
	}

	return err
}

var PosicoesDescontosEncargosPedidoCompras = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                                    {0, 2, 0},
	"PercentualDescontoComercial":                     {2, 7, 2},
	"ValorDescontoComercial":                          {7, 25, 2},
	"PercentualDescontoFinanceiro":                    {25, 30, 2},
	"ValorDescontoFinanceiro":                         {30, 48, 2},
	"PercentualDescontoPromocional":                   {48, 53, 2},
	"ValorDescontoPromocional":                        {53, 71, 2},
	"PercentualEncargoFinanceiro":                     {71, 76, 2},
	"ValorEncargoFinanceiro":                          {76, 94, 2},
	"PercentualEncargoFrete":                          {94, 99, 2},
	"ValorEncargoFrete":                               {99, 117, 2},
	"PercentualEncargoSeguro":                         {117, 122, 2},
	"ValorEncargoSeguro":                              {122, 140, 2},
	"CodigoAcertoConta":                               {140, 143, 0},
	"PercentualAbatimentoFrente":                      {143, 148, 2},
	"ValorAbatimentoFrente":                           {148, 166, 2},
	"PercentualEncargoColeta":                         {166, 171, 2},
	"ValorEncargoColeta":                              {171, 189, 2},
	"PercentualBonificacao":                           {189, 194, 2},
	"ValorBonificacao":                                {194, 212, 2},
	"PercentualMercadoriasEntregueSemPedido":          {212, 217, 2},
	"ValorMercadoriasEntregueSemPedido":               {217, 235, 2},
	"PercentualDescontoPropaganda":                    {235, 240, 2},
	"ValorDescontoPropaganda":                         {240, 258, 2},
	"PercentualDescontoDistribuidor":                  {258, 263, 2},
	"ValorDescontoDistribuidor":                       {263, 281, 2},
	"PercentualDescontoNegocioOuVolume":               {281, 286, 2},
	"ValorDescontoNegocioOuVolume":                    {286, 304, 2},
	"PercentualDescontoMercadoriasComRestricoesVenda": {304, 309, 2},
	"ValorDescontoMercadoriasComRestricoesVenda":      {309, 327, 2},
	"Filler": {327, 650, 0},
}
