package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type TextoLivre struct {
	TipoRegistro         string `json:"TipoRegistro"`
	InformacoesManuseio1 string `json:"InformacoesManuseio1"`
	InformacoesManuseio2 string `json:"InformacoesManuseio2"`
	InformacoesManuseio3 string `json:"InformacoesManuseio3"`
	InformacoesManuseio4 string `json:"InformacoesManuseio4"`
	InformacoesManuseio5 string `json:"InformacoesManuseio5"`
	Filler               string `json:"Filler"`
}

func (r *TextoLivre) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesTextoLivre

	err = posicaoParaValor.ReturnByType(&r.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.InformacoesManuseio1, "InformacoesManuseio1")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.InformacoesManuseio2, "InformacoesManuseio2")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.InformacoesManuseio3, "InformacoesManuseio3")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.InformacoesManuseio4, "InformacoesManuseio4")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.InformacoesManuseio5, "InformacoesManuseio5")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.Filler, "Filler")
	if err != nil {
		return err
	}

	return err
}

var PosicoesTextoLivre = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":         {0, 2, 0},
	"InformacoesManuseio1": {2, 72, 0},
	"InformacoesManuseio2": {72, 142, 0},
	"InformacoesManuseio3": {142, 212, 0},
	"InformacoesManuseio4": {212, 282, 0},
	"InformacoesManuseio5": {282, 352, 0},
	"Filler":               {352, 650, 0},
}
