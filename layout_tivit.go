package layout_tivit

import (
	"bitbucket.org/infarma/layout-tivit/arquivoDePedido"
	"os"
)

func GetArquivoDePedido(fileHandle *os.File) (arquivoDePedido.ArquivoDePedido, error) {
	return arquivoDePedido.GetStruct(fileHandle)
}
